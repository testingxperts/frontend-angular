FROM nginx:alpine

MAINTAINER dinesh.kumar

COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html

COPY dist/* .
